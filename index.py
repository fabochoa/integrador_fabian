from excelreader import ExcelDB

if __name__ == '__main__':
    exceldb = ExcelDB("Db.xlsx")

    sheetnames = exceldb.list_databases()
    print ("Tablas del excel")
    print (", ".join(sheetnames))
    print ("\r")

    columns = exceldb.list_columns('Aptitudes')
    print ("Columnas de la tabla Aptitudes")
    print (", ".join(columns))
    print ("\r")

    valores = exceldb.list_values_on_sheet('Aptitudes', ['ID', 'Fortalezas'])
    print ("Valores de las columnas 'ID' y 'Cedula' la tabla Aptitudes")
    print (valores)
    print ("\r")

