import xlrd

class ExcelDB:

    def __init__(self, file):
        self.dbfile = xlrd.open_workbook(file)

    def list_databases(self):
        return self.dbfile.sheet_names()

    def get_worksheet(self, sheetname):
        tables = self.list_databases()
        index = tables.index(sheetname)
        return self.dbfile.sheet_by_index(index)

    def list_columns(self, sheetname):
        worksheet = self.get_worksheet(sheetname)

        columns = []  # Header
        for col in range(worksheet.ncols):
            columns.append(worksheet.cell_value(0, col))

        return columns

    def list_values_on_sheet(self, sheetname, columns):
        worksheet = self.get_worksheet(sheetname)

        columns_indexes = []  # Header
        columns_names_indexes = []
        for col in range(worksheet.ncols):
            col_name = worksheet.cell_value(0, col)
            index_value = -1

            try:
                index_value = columns.index(col_name)
            except ValueError:
                index_value = -1

            if index_value != -1:
                columns_indexes.append(col)
                columns_names_indexes.append(index_value)

        print(columns_indexes)

        result = []
        for row in range(worksheet.nrows):
            if row > 0:
                row_data = {}

                for i in range(len(columns_indexes)):
                    col = columns_indexes[i]
                    row_data[columns[columns_names_indexes[i]]] = worksheet.cell_value(row, col)

                result.append(row_data)

        return result

    #PROYECTO DE BASE DE DATOS#
